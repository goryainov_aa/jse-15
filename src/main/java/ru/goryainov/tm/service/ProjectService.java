package ru.goryainov.tm.service;

import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name, final String description, final User user) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (user == null) return null;
        return projectRepository.create(name, description, user);
    }

    public Project update(final Long id, final String name, final String description, final Long userId) throws ProjectNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return projectRepository.update(id, description, userId);
    }

    public void clear(final Long userId) {
        projectRepository.clear(userId);
    }

    /*public Project findByIndex(int index) {
        if (index == 0 ) return null;
        return projectRepository.findByIndex(index);
    }*/

    public List<Project> findByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return projectRepository.findByName(name, userId);
    }

    public Project findById(final Long id, final Long userId) throws ProjectNotFoundException {
        if (id == null ) return null;
        if (userId == null) return null;
        return projectRepository.findById(id, userId);
    }

    public Project findById(final Long id) throws ProjectNotFoundException {
        if (id == null ) return null;
        return projectRepository.findById(id);
    }

    public Project removeById(final Long id, final Long userId) throws ProjectNotFoundException {
        if (id == null ) return null;
        if (userId == null) return null;
        return projectRepository.removeById(id, userId);
    }

    public List<Project> removeByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return projectRepository.removeByName(name, userId);
    }

    /*public Project removeByIndex(int index) {
        if (index == 0 ) return null;
        return projectRepository.removeByIndex(index);
    }*/

    public List<Project> findAll(final Long userId) {
        return projectRepository.findAll(userId);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

}
