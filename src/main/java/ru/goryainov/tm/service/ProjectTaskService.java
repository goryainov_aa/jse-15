package ru.goryainov.tm.service;

import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.entity.Task;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.exception.TaskNotFoundException;
import ru.goryainov.tm.repository.ProjectRepository;
import ru.goryainov.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> findAddByProjectId(final Long projectId, final Long userId) {
        if (projectId == null) return Collections.emptyList();
        if (userId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId, userId);
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId, final Long userId){
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId, userId);
        if (task == null) return  null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId, final Long userId) throws ProjectNotFoundException, TaskNotFoundException {
        final Project project = projectRepository.findById(projectId, userId);
        if (project == null) return null;
        final Task task = taskRepository.findById(taskId, userId);
        if (task == null) return  null;
        task.setProjectId(projectId);
        return task;
    }

}
