package ru.goryainov.tm.service;

import ru.goryainov.tm.entity.Task;
import ru.goryainov.tm.exception.TaskNotFoundException;
import ru.goryainov.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return taskRepository.create(name, userId);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return taskRepository.create(name, description, userId);
    }

    public Task update(final Long id, final String name, final String description, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return taskRepository.update(id, description, userId);
    }

    public void clear(final Long userId) {
        taskRepository.clear(userId);
    }

    /*public Task findByIndex(int index) {
        if (index == 0 ) return null;
        return taskRepository.findByIndex(index);
    }*/

    public List<Task> findByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return taskRepository.findByName(name, userId);
    }

    public Task findById(final Long id, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        if (userId == null) return null;
        return taskRepository.findById(id, userId);
    }

    public Task findById(final Long id) throws TaskNotFoundException {
        if (id == null) return null;
        return taskRepository.findById(id);
    }

    public Task removeById(final Long id, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        if (userId == null) return null;
        return taskRepository.removeById(id, userId);
    }

    public List<Task> removeByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return taskRepository.removeByName(name, userId);
    }

    /*public Task removeByIndex(int index) {
        if (index == 0 ) return null;
        return taskRepository.removeByIndex(index);
    }*/

    public List<Task> findAll(final Long userId) {
        if (userId == null) return null;
        return taskRepository.findAll(userId);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id, final Long userId) {
        if (projectId == null || id == null || userId == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id, userId);
    }

    public List<Task> findAllByProjectId(final Long projectId, final Long userId) {
        if (projectId == null || userId == null) return null;
        return taskRepository.findAllByProjectId(projectId, userId);
    }
}
