package ru.goryainov.tm;

import ru.goryainov.tm.controller.ProjectController;
import ru.goryainov.tm.controller.SystemController;
import ru.goryainov.tm.controller.TaskController;
import ru.goryainov.tm.controller.UserController;
import ru.goryainov.tm.enumerated.Role;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.exception.TaskNotFoundException;
import ru.goryainov.tm.repository.ProjectRepository;
import ru.goryainov.tm.repository.TaskRepository;
import ru.goryainov.tm.repository.UserRepository;
import ru.goryainov.tm.service.ProjectService;
import ru.goryainov.tm.service.ProjectTaskService;
import ru.goryainov.tm.service.TaskService;
import ru.goryainov.tm.service.UserService;

import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;

import static ru.goryainov.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение с поддержкой аргументов запуска
 */

public class App {

    private final UserRepository userRepository = new UserRepository();

    private final UserService userService = new UserService(userRepository);

    private final UserController userController = new UserController(userService);

    private final ProjectRepository projectRepository = new ProjectRepository(userRepository);

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService, userController);

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final TaskController taskController = new TaskController(taskService, projectTaskService, userController);

    private final SystemController systemController = new SystemController();



    private UUID sessionId;


    {
        userRepository.create("", "", "", "admin", "qwerty", Role.ADMIN);
        userRepository.create("", "", "", "user", "qwerty", Role.USER);


        /*projectRepository.create("demo project 1", "desc project 1");
        projectRepository.create("demo project 2", "desc project 1");
        taskRepository.create("test task 1", "desc task 1");
        taskRepository.create("test task 2", "desc task 2");*/
    }

    /**
     * Точка входа
     *
     * @param args параметры запуска
     */
    public static void main(final String[] args) {
        final App app = new App();
        final Scanner scanner = new Scanner(System.in);
        try {
            app.run(args);
        } catch (ProjectNotFoundException | TaskNotFoundException exception) {
            exception.printStackTrace();
        }
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            try {
                app.run(command);
                app.systemController.commandLogging(command);
            } catch (ProjectNotFoundException | TaskNotFoundException exception)
            {
                /*app.systemController.displayError(exception);*/
                System.out.println(exception.getMessage());

            }
        }
    }

    /**
     * Запуск приложения
     *
     * @param args массив параметров запуска
     */
    public void run(final String[] args) throws TaskNotFoundException, ProjectNotFoundException {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * Запуск приложения
     *
     * @param param параметр запуска
     */
    public int run(final String param) throws ProjectNotFoundException, TaskNotFoundException {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION:
                return systemController.displayVersion();
            case ABOUT:
                return systemController.displayAbout();
            case HELP:
                return systemController.displayHelp();
            case EXIT:
                return systemController.displayExit();
            case VIEW_COMMANDS_LOG:
                return systemController.displayCommandsLog();
            case PROJECT_CREATE:
                return projectController.createProject(sessionId);
            case PROJECT_CLEAR:
                return projectController.clearProject(sessionId);
            case PROJECT_LIST:
                return projectController.listProject(sessionId);
            case PROJECT_VIEW_BY_ID:
                return projectController.viewProjectById(sessionId);
            /*case PROJECT_VIEW_BY_INDEX:
                return projectController.viewProjectByIndex();*/
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName(sessionId);
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById(sessionId);
            /*case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX:
                return projectController.updateProjectByIndex();*/
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById(sessionId);
            case PROJECT_ADD_USER_BY_ID:
                return projectController.addProjectToUserId();
            case TASK_CREATE:
                return taskController.createTask(sessionId);
            case TASK_CLEAR:
                return taskController.clearTask(sessionId);
            case TASK_LIST:
                return taskController.listTask(sessionId);
            /*case TASK_VIEW_BY_INDEX:
                return taskController.viewTaskByIndex();*/
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById(sessionId);
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName(sessionId);
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById(sessionId);
           /* case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX:
                return taskController.updateTaskByIndex();*/
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById(sessionId);
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return taskController.addTaskToProjectByIds(sessionId);
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskController.removeTaskToProjectByIds(sessionId);
            case TASK_LIST_BY_PROJECT_ID:
                return taskController.listTaskByProjectId(sessionId);
            case TASK_ADD_USER_BY_ID:
                return taskController.addTaskToUserId();
            case USER_CREATE:
                return userController.createUser();
            case USER_LIST:
                return userController.listUser();
            case USER_CLEAR:
                return userController.clearUser();
            case USER_UPDATE_BY_INDEX:
                return userController.updateUserByIndex();
            case USER_UPDATE_BY_ID:
                return userController.updateUserById();
            case USER_UPDATE_BY_LOGIN:
                return userController.updateUserByLogin();
            case USER_REMOVE_BY_NAME:
                return userController.removeUserByName();
            case USER_REMOVE_BY_INDEX:
                return userController.removeUserByIndex();
            case USER_REMOVE_BY_ID:
                return userController.removeUserById();
            case USER_REMOVE_BY_LOGIN:
                return userController.removeUserByLogin();
            case USER_VIEW_BY_ID:
                return userController.viewUserById();
            case USER_VIEW_BY_INDEX:
                return userController.viewUserByIndex();
            case USER_VIEW_BY_LOGIN:
                return userController.viewUserByLogin();
            case USER_AUTHENTIFICATION:
                sessionId = userController.authentification();
                if (sessionId != null) systemController.newSession();
                return 0;
            case USER_LOGOFF:
                userController.logOff();
                return 0;
            case USER_CHANGE_PASSWORD:
                userController.changePasswordByUserLogin(sessionId);
                return 0;
            default:
                return systemController.displayError();
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
}
