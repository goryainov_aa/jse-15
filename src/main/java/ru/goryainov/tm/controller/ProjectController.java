package ru.goryainov.tm.controller;

import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.service.ProjectService;
import ru.goryainov.tm.service.UserService;

import java.util.*;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;
    private final UserController userController;

    public ProjectController(ProjectService projectService, UserController userController) {
        this.projectService = projectService;
        this.userController = userController;
    }

    /**
     * Cоздания проекта
     */
    public int createProject(final UUID sessionId) {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Create project]");
        System.out.println("[Please, enter project name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanNextLine();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        User user_prj = userController.getUserById(userId);
        projectService.create(name, description, user_prj);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение проекта по индексу
     */
    /*public int updateProjectByIndex() {
        System.out.println("[Update project]");
        System.out.println("[Please, enter project index:]");
        final int index = Integer.parseInt(scanNextLine()) - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter project name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanNextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }*/

    /**
     * Изменение проекта по идентификатору
     */
    public int updateProjectById(final UUID sessionId) throws ProjectNotFoundException {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Update project]");
        System.out.println("[Please, enter project id:]");
        final long id = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Project project = projectService.findById(id, userId);
        System.out.println("[Please, enter project name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanNextLine();
        projectService.update(project.getId(), name, description, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по имени
     */
    public int removeProjectByName(final UUID sessionId) {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Remove project by name]");
        System.out.println("[Please, enter project name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final List<Project> project = projectService.removeByName(name, userId);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по идентификатору
     */
    public int removeProjectById(final UUID sessionId) throws ProjectNotFoundException {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Remove project by id]");
        System.out.println("[Please, enter project id:]");
        final long id = scanNextLong();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Project project = projectService.removeById(id, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по индексу
     */
    /*public int removeProjectByIndex() {
        System.out.println("[Remove project by index]");
        System.out.println("[Please, enter project index:]");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }*/

    /**
     * Вывод очистки проекта
     */
    public int clearProject(final UUID sessionId) {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Clear project]");
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        projectService.clear(userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Просмотр проекта по индексу
     */
    /*public int viewProjectByIndex() {
        System.out.println("Enter, project index: ");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }*/

    /**
     * Просмотр проекта по идентификатору
     */
    public int viewProjectById(final UUID sessionId) throws ProjectNotFoundException {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("Enter, project id: ");
        final Long id = scanNextLong();
        User user = userController.getUserBySession(sessionId);
        if (user == null) return 1;
        final Project project = projectService.findById(id, user.getId());
        if (checkAdminGrants(user) || user.getId().equals(project.getUserId())) {
            viewProject(project);
            return 0;
        } else System.out.println("You don't have enough privileges!");
        return 0;
    }


    /**
     * Просмотр списка проектов
     */
    private void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[View project]");
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("UserId: " + project.getUserId());
        System.out.println("[Ok]");
    }

    /**
     * Вывод списка проектов
     */
    public int listProject(final UUID sessionId) {
        User user = userController.getUserBySession(sessionId);
        if (user == null) return 1;
        List<Project> projectList = null;
        if (checkAdminGrants(user)) projectList = projectService.findAll();
        else projectList = projectService.findAll(user.getId());
        Collections.sort(projectList, Comparator.comparing(Project::getName));
        System.out.println("[List project]");
        int index = 1;
        for (final Project project : projectList) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName() + " description " + project.getDescription() + " user: " + userController.getUserById(project.getUserId()).getLogin());
            index++;
        }
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Назначение проекта пользователю
     */
    public int addProjectToUserId() throws ProjectNotFoundException {
        System.out.println("[Add project to user]");
        System.out.println("Enter, project id: ");
        final Long projectId = scanNextLong();
        System.out.println("Enter, user id: ");
        final Long userId = scanNextLong();
        if (userId == null || projectId == null) return 1;
        Project project;
        project = projectService.findById(projectId);
        project.setUserId(userId);
        System.out.println("[Ok]");
        return 0;
    }
}
