package ru.goryainov.tm.controller;


import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.entity.Task;
import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.exception.ProjectNotFoundException;
import ru.goryainov.tm.exception.TaskNotFoundException;
import ru.goryainov.tm.service.ProjectTaskService;
import ru.goryainov.tm.service.TaskService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    private final UserController userController;


    public TaskController(TaskService taskService, ProjectTaskService projectTaskService, UserController userController) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.userController = userController;
    }

    /**
     * Вывод создания задачи
     */
    public int createTask(final UUID sessionId) {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Create task]");
        System.out.println("[Please, enter task name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter task description:]");
        final String description = scanNextLine();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        taskService.create(name, description, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение задачи по индексу
     */
    /*public int updateTaskByIndex() {
        System.out.println("[Update task]");
        System.out.println("[Please, enter task index:]");
        final int index = Integer.parseInt(scanNextLine()) - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter task name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter task description:]");
        final String description = scanNextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }*/

    /**
     * Изменение задачи по идентификатору
     */
    public int updateTaskById(final UUID sessionId) throws TaskNotFoundException {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Update task]");
        System.out.println("[Please, enter task id:]");
        final long id = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Task task = taskService.findById(id, userId);
        if (task == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter task name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter task description:]");
        final String description = scanNextLine();
        taskService.update(task.getId(), name, description, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Очистка задачи
     */
    public int clearTask(final UUID sessionId) {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Clear task]");
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        taskService.clear(userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по имени
     */
    public int removeTaskByName(final UUID sessionId) {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Remove task by name]");
        System.out.println("[Please, enter task name:]");
        final String name = scanNextLine();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final List<Task> task = taskService.removeByName(name, userId);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по идентификатору
     */
    public int removeTaskById(final UUID sessionId) throws TaskNotFoundException {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Remove task by id]");
        System.out.println("[Please, enter task id:]");
        final long id = scanNextLong();
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        final Task task = taskService.removeById(id, userId);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из списка по индексу
     */
    /*public int removeTaskByIndex() {
        System.out.println("[Remove task by index]");
        System.out.println("[Please, enter task index:]");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }*/

    /**
     * Просмотр задачи по индексу
     */
    /*public int viewTaskByIndex() {
        System.out.println("Enter, task index: ");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }*/

    /**
     * Просмотр задачи по идентификатору
     */
    public int viewTaskById(final UUID sessionId) throws TaskNotFoundException {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("Enter, task id: ");
        final long id = scanNextLong();
        User user = userController.getUserBySession(sessionId);
        final Task task = taskService.findById(id, user.getId());
        if (checkAdminGrants(user) || user.getId().equals(task.getUserId())) {
            viewTask(task);
            return 0;
        } else System.out.println("You don't have enough privileges!");
        return 0;
    }

    /**
     * Просмотр списка задач
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[View task]");
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("[Ok]");
    }

    /**
     * Вывод списка задач
     */
    /*public int listTask(final UUID sessionId) {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[List task]");
        User user = userController.getUserBySession(sessionId);
        if (user == null) return 1;
        viewTasks(taskService.findAll(user.getId()));
        System.out.println("[Ok]");
        return 0;
    }*/

    public int listTask(final UUID sessionId) {
        User user = userController.getUserBySession(sessionId);
        System.out.println("[List task]");
        if (checkAdminGrants(user)) viewTasks(taskService.findAll());
        else viewTasks(taskService.findAll(user.getId()));
        System.out.println("[Ok]");
        return 0;
    }


    /**
     * Вывод списка задач
     */
    private void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        Collections.sort(tasks, Comparator.comparing(Task::getName));
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName() + "description:" + task.getDescription() + " user: " + userController.getUserById(task.getUserId()).getLogin());
            index++;
        }
    }

    /**
     * Вывод списка задач в проекте по id
     */
    public int listTaskByProjectId(final UUID sessionId) {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[List task by project]");
        System.out.println("[Please, enter project id:]");
        final long projectId = Long.parseLong(scanNextLine());
        User user = userController.getUserBySession(sessionId);
        final List<Task> tasks = taskService.findAllByProjectId(projectId, user.getId());
        viewTasks(tasks);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Добавление задачи в проект по id
     */
    public int addTaskToProjectByIds(final UUID sessionId) throws TaskNotFoundException, ProjectNotFoundException {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Add task to project by ids]");
        System.out.println("[Please, enter project id:]");
        final long projectId = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter task id:]");
        final long taskId = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        projectTaskService.addTaskToProject(projectId, taskId, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление задачи из проект по id
     */
    public int removeTaskToProjectByIds(final UUID sessionId) {
        if (!userController.checkSession(sessionId)) return 1;
        System.out.println("[Remove task from project by ids]");
        System.out.println("[Please, enter project id:]");
        final long projectId = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter task id:]");
        final long taskId = Long.parseLong(scanNextLine());
        System.out.println("[Please, enter user Id:]");
        final Long userId = scanNextLong();
        projectTaskService.removeTaskFromProject(projectId, taskId, userId);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Назначение задачи пользователю
     */
    public int addTaskToUserId() throws TaskNotFoundException {
        System.out.println("[Add task to user]");
        System.out.println("Enter, task id: ");
        final Long taskId = scanNextLong();
        System.out.println("Enter, user id: ");
        final Long userId = scanNextLong();
        if (userId == null || taskId == null) return 1;
        Task task;
        task = taskService.findById(taskId);
        task.setUserId(userId);
        System.out.println("[Ok]");
        return 0;
    }

}
