package ru.goryainov.tm.repository;
import ru.goryainov.tm.entity.Task;
import ru.goryainov.tm.exception.TaskNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();
    private HashMap<String, List<Task>> task_map = new HashMap();

    private void save(Task task, String name){
        List<Task>  lst;
        tasks.add(task);
        lst = task_map.get(name);
        if(lst == null)
        {
            lst = new ArrayList<>();
            task_map.put(name, lst);
        }
        lst.add(task);
    }

    public Task create(final String name, final Long userId) {
        final Task task = new Task(name);
        task.setUserId(userId);
        save(task, name);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name);
        task.setDescription(description);
        save(task, name);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task(name);
        task.setDescription(description);
        task.setUserId(userId);
        save(task, name);
        return task;
    }

    public Task update(final Long id, final String description, final Long userId) throws TaskNotFoundException {
        final Task task = findById(id, userId);
        if (task == null) return null;
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    public void clear(final Long userId) {
        List<Task> userTasks = findByUserId(userId);
        for (Task task : userTasks) {
            tasks.remove(task);
            task_map.get(task.getName()).remove(task);
        }

    }

    public List<Task> findByUserId(final Long userId) {
        List<Task> userTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (task.getUserId() == null || !task.getUserId().equals(userId)) continue;
            userTasks.add(task);
        }
        return userTasks;
    }


    /*public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size() - 1) return null;
        return tasks.get(index);
    }*/

    public List<Task> findByName(final String name, final Long userId) {
        if (name == null || name.isEmpty() || userId == null) return null;
        List<Task> result = new ArrayList<>();
        for (final Task task : task_map.get(name)) {
            /*if (!task.getName().equals(name) || !task.getUserId().equals(userId)) continue;
            * return task*/
            if(task.getUserId().equals(userId)) result.add(task);
        }
        return result;
    }

    public Task findById(final Long id, final Long userId) throws TaskNotFoundException {
        if (id == null || userId == null) return null;
        for (final Task task : tasks) {
            if (!task.getId().equals(id) || !task.getUserId().equals(userId)) continue;
            return task;
        }
        throw new TaskNotFoundException("Task is not found by id " + id + ".");
    }

    public Task findById(final Long id) throws TaskNotFoundException {
        if (id == null) return null;
        for (final Task task : tasks) {
            if (!task.getId().equals(id)) continue;
            return task;
        }
        throw new TaskNotFoundException("Task is not found by id " + id + ".");
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id, final Long userId) {
        if (id == null || projectId == null || userId == null) return null;
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getUserId() == null || !task.getUserId().equals(userId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeById(final Long id, final Long userId) throws TaskNotFoundException {
        final Task task = findById(id, userId);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public List<Task> removeByName(final String name, final Long userId) {
        final List<Task> task = findByName(name, userId);
        if (task == null || task.isEmpty()) return null;
        tasks.remove(task);
        task_map.remove(task);
        return task;
    }

    /*public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }*/

    public List<Task> findAllByProjectId(final Long projectId, final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll(userId)) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAll(final Long userId) {
        List<Task> userTasks = findByUserId(userId);
        return userTasks;
    }
    public List<Task> findAll() {
        return tasks;
    }
}
