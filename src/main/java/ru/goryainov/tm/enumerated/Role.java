package ru.goryainov.tm.enumerated;

import ru.goryainov.tm.controller.SystemController;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

    public static void main(String[] args) {
        System.out.println(Role.USER.name());
        System.out.println(Role.USER.getDisplayName());
        System.out.println(Role.USER);
    }
}
