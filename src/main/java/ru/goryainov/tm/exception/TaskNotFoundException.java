package ru.goryainov.tm.exception;

public class TaskNotFoundException extends Exception{
    public TaskNotFoundException(String errMsg){
        super(errMsg);
    }
}
